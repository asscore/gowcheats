#include <erl.h>
#include <iopcontrol.h>
#include <iopheap.h>
#include <loadfile.h>
#include <kernel.h>
#include <sbv_patches.h>
#include <sifrpc.h>
#include <stdio.h>
#include <string.h>
#include <tamtypes.h>
#include <unistd.h>

/* == Resets the IOP ====================================== */

void reset_iop() {
    printf("Reseting IOP...\n");

    SifInitRpc(0);

    while (!SifIopReset("rom0:UDNL rom0:EELOADCNF", 0));
    while (!SifIopSync());

    /* exit services */
    fioExit();
    SifExitIopHeap();
    SifLoadFileExit();
    SifExitRpc();
    SifExitCmd();

    FlushCache(0);
    FlushCache(2);

    /* init services */
    SifInitRpc(0);
    SifLoadFileInit();
    SifInitIopHeap();
    fioInit();

    FlushCache(0);
    FlushCache(2);
}

/* == Load initial modules ================================ */

extern u8  _iomanX_irx_start[];
extern int _iomanX_irx_size;
extern u8  _usbd_irx_start[];
extern int _usbd_irx_size;
extern u8  _usbhdfsd_irx_start[];
extern int _usbhdfsd_irx_size;

void load_modules() {
    int ret;
    printf("Loading main modules...\n");

    SifLoadModule("rom0:SIO2MAN", 0, NULL);
    SifLoadModule("rom0:PADMAN", 0, NULL);
    SifLoadModule("rom0:MCMAN", 0, NULL);
    SifLoadModule("rom0:MCSERV", 0, NULL);
    SifExecModuleBuffer(_iomanX_irx_start, _iomanX_irx_size, 0, NULL, &ret);
    SifExecModuleBuffer(_usbd_irx_start, _usbd_irx_size, 0, NULL, &ret);
    SifExecModuleBuffer(_usbhdfsd_irx_start, _usbhdfsd_irx_size, 0, NULL, &ret);

    sleep(1);  /* allow USB devices some time to be detected */
}

/* == Install cheat engine ERL ============================ */

extern unsigned char _engine_erl_start[];

int (*get_max_hooks)(void);
int (*get_num_hooks)(void);
int (*add_hook)(u32 addr, u32 val);
void (*clear_hooks)(void);

int (*get_max_codes)(void);
void (*set_max_codes)(int num);
int (*get_num_codes)(void);
int (*add_code)(u32 addr, u32 val);
void (*clear_codes)(void);

void setup_erl() {
   struct erl_record_t *erl;

    erl_add_global_symbol("GetSyscallHandler", (u32)GetSyscallHandler);
    erl_add_global_symbol("SetSyscall", (u32)SetSyscall);

    /* install cheat engine ERL */
    erl = load_erl_from_mem_to_addr(_engine_erl_start, 0x00080000, 0, NULL);
    if(!erl) {
        printf("Error loading cheat engine ERL!\n");
        SleepThread();
    }

    erl->flags |= ERL_FLAG_CLEAR;
    FlushCache(0);

    printf("Installed cheat engine ERL. Getting symbols...\n");
    struct symbol_t *sym;
    #define GET_SYMBOL(var, name) \
    sym = erl_find_local_symbol(name, erl); \
    if (sym == NULL) { \
        printf("%s: could not find symbol '%s'\n", __FUNCTION__, name); \
        SleepThread(); \
    } \
    printf("%08x %s\n", (u32)sym->address, name); \
    var = (typeof(var))sym->address

    GET_SYMBOL(get_max_hooks, "get_max_hooks");
    GET_SYMBOL(get_num_hooks, "get_num_hooks");
    GET_SYMBOL(add_hook, "add_hook");
    GET_SYMBOL(clear_hooks, "clear_hooks");
    GET_SYMBOL(get_max_codes, "get_max_codes");
    GET_SYMBOL(set_max_codes, "set_max_codes");
    GET_SYMBOL(get_num_codes, "get_num_codes");
    GET_SYMBOL(add_code, "add_code");
    GET_SYMBOL(clear_codes, "clear_codes");

    printf("Symbols loaded.\n");
}

/* == Load active cheats ================================== */

void activate_cheat() {
    printf("Setting up codes for code engine...\n");

    /* Enable Code (Must Be On) */
    add_hook(0x9027BA70, 0x0C0A24A5);
    /* Infinite Health */
    add_code(0xE002453B, 0x1079597E);
    add_code(0x5079597C, 0x00000004);
    add_code(0x00795978, 0x00000000);
    /* Press L1+Left To Refill Health + Quick Last Boss Death */
    add_code(0xE002FB7F, 0x002FDA9C);
    add_code(0xE001453B, 0x0079597E);
    add_code(0x20795978, 0x453B7FFF);
    /* Infinite Magic */
    add_code(0xE0020000, 0x10302D42);
    add_code(0x50302D18, 0x00000004);
    add_code(0x00302D1C, 0x00000000);

    printf("Done setting up codes.\n");
}

/* == Load a game ========================================= */

/* ELF File Header */
typedef struct {
    u8  ident[16];  /* Magic number and other info */
    u16 type;       /* Object file type */
    u16 machine;    /* Architecture */
    u32 version;    /* Object file version */
    u32 entry;      /* Entry point virtual address */
    u32 phoff;      /* Program header table file offset */
    u32 shoff;      /* Section header table file offset */
    u32 flags;      /* Processor-specific flags */
    u16 ehsize;     /* ELF header size in bytes */
    u16 phentsize;  /* Program header table entry size */
    u16 phnum;      /* Program header table entry count */
    u16 shentsize;  /* Section header table entry size */
    u16 shnum;      /* Section header table entry count */
    u16 shstrndx;   /* Section header string table index */
} elf_header_t;

/* Program segment header */
typedef struct {
    u32 type;     /* Segment type */
    u32 offset;   /* Segment file offset */
    void *vaddr;  /* Segment virtual address */
    u32 paddr;    /* Segment physical address */
    u32 filesz;   /* Segment size in file */
    u32 memsz;    /* Segment size in memory */
    u32 flags;    /* Segment flags */
    u32 align;    /* Segment alignment */
} elf_pheader_t;

extern u8 _bootstrap_elf_start[];
extern int _bootstrap_elf_size;

#define ELF_PT_LOAD 1

/* Load boostrap into memory. Returns bootstrap entrypoint. */
static void* load_bootstrap() {
    elf_header_t *eh = (elf_header_t *)_bootstrap_elf_start;
    elf_pheader_t *eph = (elf_pheader_t *)(_bootstrap_elf_start + eh->phoff);

    int i;
    for (i = 0; i < eh->phnum; i++)
    {
        if (eph[i].type != ELF_PT_LOAD)
            continue;

        void *pdata = (void *)(_bootstrap_elf_start + eph[i].offset);
        memcpy(eph[i].vaddr, pdata, eph[i].filesz);

        if (eph[i].memsz > eph[i].filesz)
            memset(eph[i].vaddr + eph[i].filesz, 0, eph[i].memsz - eph[i].filesz);
    }

    return (void *)eh->entry;
}

void execute(const char *path) {
    static char boot2[100];

    strncpy(boot2, path, 100);

    void *bootstrap_entrypoint = load_bootstrap();

    fioExit();
    SifInitRpc(0);
    SifExitRpc();

    FlushCache(0);  /* data cache */
    FlushCache(2);  /* instruction cache */

    char *argv[2] = {boot2, "\0"};

    ExecPS2(bootstrap_entrypoint, 0, 2, argv);
}

/* == Main ================================================ */

int main(int argc, char *argv[]) {

    if (argc != 0) {
        reset_iop();
    }

    printf("Applying SBV patches...\n");
    sbv_patch_enable_lmb();
    sbv_patch_disable_prefix_check();

    load_modules();

    setup_erl();
    activate_cheat();

    execute("mass:/BOOT/BOOT.ELF");

    SleepThread();

    return 0;
}
