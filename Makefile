#
# GoWCheats
#

EE_BIN = gowcheats.elf

EE_CFLAGS +=

# Main
OBJS += src/loader.o

# IRX Modules
IRX_OBJS += resources/usbd_irx.o
IRX_OBJS += resources/usbhdfsd_irx.o
IRX_OBJS += resources/iomanX_irx.o

# Engine
OBJS += engine/engine_erl.o

# Bootstrap ELF
OBJS += bootstrap/bootstrap_elf.o

EE_LIBS += -lpatches -lerl
EE_LDFLAGS += -L$(PS2SDK)/ee/lib -s
EE_INCS +=

EE_OBJS = $(IRX_OBJS) $(OBJS)

all: modules main

modules:
	@# IRX Modules
	@bin2o $(PS2SDK)/iop/irx/iomanX.irx resources/iomanX_irx.o _iomanX_irx
	@bin2o $(PS2SDK)/iop/irx/usbd.irx resources/usbd_irx.o _usbd_irx
	@bin2o $(PS2SDK)/iop/irx/usbhdfsd.irx resources/usbhdfsd_irx.o _usbhdfsd_irx

	@# Engine
	@cd engine && $(MAKE)
	@bin2o engine/engine.erl engine/engine_erl.o _engine_erl

	@# Bootstrap
	@cd bootstrap && $(MAKE)
	@bin2o bootstrap/bootstrap.elf bootstrap/bootstrap_elf.o _bootstrap_elf

main: $(EE_BIN)
	rm -rf src/*.o
	rm -f resources/*.o
	rm -f bootstrap/*.elf bootstrap/*.o
	rm -f engine/*.erl engine/*.o

release: all
	rm -rf release
	mkdir release
	ps2-packer $(EE_BIN) release/$(EE_BIN)

clean:
	rm -rf src/*.o *.elf
	rm -f resources/*.o
	cd engine && make clean
	cd bootstrap && make clean

run:
	ps2client reset; sleep 3
	ps2client execee host:$(EE_BIN)

include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
