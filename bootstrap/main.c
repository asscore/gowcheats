#include <iopcontrol.h>
#include <iopheap.h>
#include <kernel.h>
#include <loadfile.h>
#include <sifrpc.h>
#include <stdio.h>
#include <string.h>
#include <tamtypes.h>

/* Debug colors. Now NTSC Safe! At least I believe they are... */
int red = 0x1010B4;     /* RED: Opening elf */
int green = 0x10B410;   /* GREEN: Reading elf */
int blue = 0xB41010;    /* BLUE: Installing elf header */
int yellow = 0x10B4B4;  /* YELLOW: ExecPS2 */
int orange = 0x0049E6;  /* ORANGE: SifLoadElf */
int pink = 0xB410B4;    /* PINK: Launching OSDSYS */
int white = 0xEBEBEB;   /* WHITE: Failed to launch OSDSYS */
int purple = 0x801080;  /* PURPLE: Launching uLaunchELF */
int black = 0x101010;

#define ELF_MAGIC   0x464c457f
#define ELF_PT_LOAD 1

int g_argc;
char *g_argv[16];

#define GS_BGCOLOUR *((vu32*)0x120000e0)

struct _lf_elf_load_arg {
    union {
        u32 epc;
        int result;
    } p;
    u32 gp;
    char path[LF_PATH_MAX];
    char secname[LF_ARG_MAX];
} __attribute__((aligned (16)));

/* ELF File Header */
typedef struct {
    u8  ident[16];  /* Magic number and other info */
    u16 type;       /* Object file type */
    u16 machine;    /* Architecture */
    u32 version;    /* Object file version */
    u32 entry;      /* Entry point virtual address */
    u32 phoff;      /* Program header table file offset */
    u32 shoff;      /* Section header table file offset */
    u32 flags;      /* Processor-specific flags */
    u16 ehsize;     /* ELF header size in bytes */
    u16 phentsize;  /* Program header table entry size */
    u16 phnum;      /* Program header table entry count */
    u16 shentsize;  /* Section header table entry size */
    u16 shnum;      /* Section header table entry count */
    u16 shstrndx;   /* Section header string table index */
} elf_header_t;

/* Program segment header */
typedef struct {
    u32 type;     /* Segment type */
    u32 offset;   /* Segment file offset */
    void *vaddr;  /* Segment virtual address */
    u32 paddr;    /* Segment physical address */
    u32 filesz;   /* Segment size in file */
    u32 memsz;    /* Segment size in memory */
    u32 flags;    /* Segment flags */
    u32 align;    /* Segment alignment */
} elf_pheader_t;

/* From NetCheat */
void MyLoadElf(char *elfpath)
{
    ResetEE(0x7f);

    /* clear user memory */
    int i;
    for (i = 0x00100000; i < 0x02000000; i += 64) {
        __asm__ (
        "\tsq $0, 0(%0) \n"
        "\tsq $0, 16(%0) \n"
        "\tsq $0, 32(%0) \n"
        "\tsq $0, 48(%0) \n"
        :: "r" (i)
        );
    }

    /* clear scratchpad memory */
    memset((void*)0x70000000, 0, 16 * 1024);

    /* hack do not reset IOP when launching ELF from mass */
    if (!(elfpath[0] == 'm' && elfpath[1] == 'a' &&
            elfpath[2] == 's' && elfpath[3] == 's')) {
        /* reset IOP */
        SifInitRpc(0);

        FlushCache(0);
        FlushCache(2);

        /* reload modules */
        SifLoadFileInit();
        SifLoadModule("rom0:SIO2MAN", 0, NULL);
        SifLoadModule("rom0:MCMAN", 0, NULL);
        SifLoadModule("rom0:MCSERV", 0, NULL);
    }

    char *args[1];
    t_ExecData sifelf;
    memset(&sifelf, 0, sizeof(t_ExecData));

    /* try to load the ELF with SifLoadElf() first */
    int ret = SifLoadElf(elfpath, &sifelf);
    if(!ret && sifelf.epc) {
        /* exit services */
        fioExit();
        SifLoadFileExit();
        SifExitIopHeap();
        SifExitRpc();

        FlushCache(0);
        FlushCache(2);

        /* finally, run game ELF... */
        ExecPS2((void*)sifelf.epc, (void*)sifelf.gp, 1, &elfpath);

        SifInitRpc(0);
    }

    /* SifLoadElf failed, so try to load the ELF manually */
    elf_header_t boot_header;
    elf_pheader_t boot_pheader;
    int fd = 0;
    fioInit();
    GS_BGCOLOUR = red;  /* RED: Opening ELF */
    fd = fioOpen(elfpath, O_RDONLY);  /* open the ELF */
    if (fd < 0) { fioClose(fd); return; }  /* if it doesn't exist, return */

    GS_BGCOLOUR = blue;

    if(read(fd, &boot_header, sizeof(elf_header_t)) != sizeof(elf_header_t)) {
        close(fd);
    }

    GS_BGCOLOUR = white;

    /* check ELF magic */
    if ((*(u32*)boot_header.ident) != ELF_MAGIC) {
        close(fd);
    }

    /* copy loadable program segments to RAM */
    for (i = 0; i < boot_header.phnum; i++) {
        lseek(fd, boot_header.phoff+(i*sizeof(elf_pheader_t)), SEEK_SET);
        read(fd, &boot_pheader, sizeof(elf_pheader_t));

        if (boot_pheader.type != ELF_PT_LOAD)
            continue;

        lseek(fd, boot_pheader.offset, SEEK_SET);
        read(fd, boot_pheader.vaddr, boot_pheader.filesz);

        if (boot_pheader.memsz > boot_pheader.filesz)
            memset(boot_pheader.vaddr + boot_pheader.filesz, 0,
                    boot_pheader.memsz - boot_pheader.filesz);
    }

    close(fd);

    /* booting part */
    GS_BGCOLOUR = yellow;  /* YELLOW: ExecPS2 */

    /* IOP reboot routine from ps2rd */
    SifInitRpc(0);

    while (!SifIopReset("rom0:UDNL rom0:EELOADCNF", 0));
    while (!SifIopSync());

    /* exit services */
    fioExit();
    SifExitIopHeap();
    SifLoadFileExit();
    SifExitRpc();
    SifExitCmd();

    FlushCache(0);
    FlushCache(2);

    args[0] = elfpath;
    ExecPS2((u32*)boot_header.entry, 0, 1, args);
}

/*
  argv[0] = elf path
*/

int main(int argc, char *argv[]) {
    MyLoadElf((char *) argv[0]);

    return 0;
}
